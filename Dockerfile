FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-tags depName=zadam/trilium versioning=semver extractVersion=^v(?<version>.+)$
ARG TRILIUM_VERSION=0.63.7

RUN curl -L https://github.com/zadam/trilium/releases/download/v${TRILIUM_VERSION}/trilium-linux-x64-server-${TRILIUM_VERSION}.tar.xz | tar -Jxvf - --strip-components 1 -C /app/code

COPY start.sh /app/pkg

CMD [ "/app/pkg/start.sh" ]
