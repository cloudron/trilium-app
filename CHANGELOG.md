[0.1.0]
* Initial version

[1.0.0]
* Update Trilium to 0.43.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.43.2)
* fix SVG single file import as image and not as file, closes #1114 
* fix unescaped HTML in the tree node title, closes #1127 
* hide "type around" controls in printed PDF, closes #1129 
* upgrade ckeditor to 20.0.0

[1.0.1]
* Update Trilium to 0.43.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.43.3)
* fix import of initial demo document sets sync.isSynced incorrectly, #1163 
* fix extracting base64 inline images from HTML, fixes #1159 
* fix checking affected notes when modified attribute's owning note is not loaded into cache, #803
* update to CKEditor 21 which brings various improvements

[1.0.2]
* Update Trilium to 0.43.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.43.4)
* activate PDF preview also in server build, fixes #1208
* add fallback when resizing image fails, closes #1190
* fix toggle sidebar issues, closes #1196
* add "search in note" to "note actions" menu, #1184
* Add web app manifest (#1174)
* add scrolling margins, #1181
* support horizontal line, closes #1164

[1.1.0]
* Update Trilium to 0.44.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.44.4)
* 0.44.X contains changes in sync protocol compared to 0.43.X so all instances must be updated.
* major overhaul of search syntax and search capabilities
    * **new syntax is not compatible with the old one and there's no automatic migration for e.g. Saved Search notes or when searching in scripts. It's up to you to migrate the searches into the new syntax**
    * searching is unified across for all use cases - e.g. now Jump To dialog uses the same algorithms as the main search
* major overhaul of attribute UI
* imported notes are sanitized for unknown HTML/CSS/JS content
* Links in text notes behave now consistently between classical links and hypertext - single click will activate the target note
* Jump To dialog remembers (for 2 minutes) last searched item

[1.1.1]
* Update Trilium to 0.44.5
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.44.5)

[1.1.2]
* Update Trilium to 0.44.6
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.44.6)

[1.1.3]
* Update Trilium to 0.44.8
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.44.8)

[1.1.4]
* Update Trilium to 0.44.9
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.44.9)

[1.2.0]
* Update Trilium to 0.45.1
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.45.1)

[1.2.1]
* Update Trilium to 0.45.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.45.2)

[1.2.2]
* Update Trilium to 0.45.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.45.3)

[1.2.3]
* Update Trilium to 0.45.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.45.4)

[1.2.4]
* Update Trilium to 0.45.5
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.45.5)

[1.2.5]
* Update Trilium to 0.45.6
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.45.6)

[1.2.6]
* Update Trilium to 0.45.7
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.45.7)

[1.2.7]
* Update Trilium to 0.45.8
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.45.8)

[1.3.0]
* Update Trilium to 0.45.9
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.45.9)
* Use base image v3
* fix include note rendering over floating elements, #1590 
* hide note paths dropdown on changed note, #1572 
* fix/improve behavior of "sorted" attribute
* select attr name in attr detail dialog did not trigger update, closes #1557
* added "safe mode" environment variable switch which disables startup scripts

[1.3.1]
* Update Trilium to 0.45.10
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.45.10)

[1.4.0]
* Update Trilium to 0.46.5
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.46.5)
* **0.46.X contains changes in sync protocol compared to 0.45.X so all instances must be updated.**
* new search UI
  * quick search widget in the upper right corner
  * "full" search which is actually a saved search - allows to specify some search criteria using UI
  * besides searching this provides the ability to define "search actions" which allow to operate on the search results - e.g. add a label
* more intelligent "auto book" behavior where any text/code note is auto book even if it has content
* icon picker
* note tree loading now doesn't access database, instead it leverages already existing backend note cache. This results in significant speed up of some operations

[1.4.1]
* Update Trilium to 0.46.6
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.46.6)

[1.4.2]
* Update Trilium to 0.46.7
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.46.7)
* #1810 [Bug] Strange behaviour for protected notes titles

[1.4.3]
* Update Trilium to 0.46.9
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.46.9)
* use entity changes instead of actual tables to fill in sector to sync #1809
* fix lexer to parse correctly quoted empty strings, #1825
* improved task manager with allowed year range, #1823
* fix inheriting inheritable attributes through template, #1828
* ctrl + click / middle click now opens image in a text note in a new tab
* clipper fix, #1840
* use official gh client instead of github-release to execute releases
* clear note selection anytime a new note is activated
* fix moving notes with keyboard on Firefox #1865

[1.4.4]
* Update Trilium to 0.47.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.47.2)
* delete notes dialog has a preview of what notes will be deleted and broken relations
* new sync status widget next to the global menu
* empty new tab shows a list of workspaces to quickly enter
* image notes can be zoomed and panned with mousewheel/dragging

[1.4.5]
* Update Trilium to 0.47.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.47.3)

[1.4.6]
* Update Trilium to 0.47.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.47.4)

[1.4.7]
* Update Trilium to 0.47.5
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.47.5)

[1.4.8]
* Update Trilium to 0.47.7

[1.4.9]
* Update Trilium to 0.47.8
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.47.8)

[1.5.0]
* Update Trilium to 0.48.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.48.2)
* Set default icon for clipped notes by @sigaloid in https://github.com/zadam/trilium/pull/2246
* hide tooltip after clicking on a button, fixes #2228
* fix backend API's getNote, getBranch, getAttribute, closes #2230
* fix restoring note revision, closes #2232
* allow async scripts in runOnBackend(), fixes #2243

[1.5.1]
* Update Trilium to 0.48.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.48.3)
* set iconClass also on whole page clipping #2250
* fix repeated invocation of actions in note actions dropdown, closes #2255
* fix saving images from web clipper, closes #2249
* display properly relations in note map even if they are not in the subtree, #2251
* fix saving images from web clipper, closes #2249

[1.5.2]
* Update Trilium to 0.48.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.48.4)

[1.5.3]
* Update Trilium to 0.48.5
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.48.5)

[1.5.4]
* Update Trilium to 0.48.6
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.48.6)

[1.5.5]
* Update Trilium to 0.48.7
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.48.7)

[1.5.6]
* Update Trilium to 0.48.8
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.48.8)

[1.5.7]
* Update Trilium to 0.48.9
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.48.9)

[1.5.8]
* Update Trilium to 0.49.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.49.3)

[1.5.9]
* Update Trilium to 0.49.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.49.4)
* it's now possible to share (read-only) notes publicly. See wiki
* number of backlinks to the current note are shown in the note detail

[1.5.10]
* Update Trilium to 0.49.5
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.49.5)
* changed shared_info class names to avoid conflict with adblocker, closes #2546
* print should include promoted attributes, #2550
* migration workaround for corrupted index in entity_changes table, closes #2534
* make getAncestors safe in respect to saved search
* ignore "Sync not configured" message when toggling shared state
* prevent infinite recursion when checking shared status
* start sync immediately after sharing/unsharing, fixes #2539

[1.5.11]
* Update Trilium to 0.50.1
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.50.1)
* brand new external REST API called ETAPI
* removed username from authentication, only password is now needed
* removed setting password in the initial setup to speed up the process and avoid confusing users, it's possible to set it up later
* added possibility to reset password (thus losing protected notes, if any) from Options.
* as a result of above, typical "new document" setup is just two-click process without having to enter username & password
* optimized sync protocol which can now in some cases halve the sync/time
* unified Jump-To and quick search behavior with regards to hoisting and opening in a new tab, #2483
* search immediately when user chooses "full search" from Jump To dialog, #2483
* make whole text/code detail clickable to improve UX, add placeholder to empty code notes
* print should include promoted attributes, #2550
* add #shareRoot label to define an "index" note, closes #2567
* initial theme is chosen based on OS settings, #2516
* focus promoted/owned attributes input after activating the ribbon tab, closes #2606

[1.5.12]
* Update Trilium to 0.50.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.50.2)
* fix exporting huge text notes (export would get stuck on HTML pretty print)
* introduced mutex to avoid random conflicts during rapid tab closing
* fix updating becca after undeleting notes
* fixed background color of input label in CKEditor search & replace, #2254
* fix multiple Etapi token options UI bugs, closes #2616

[1.5.13]
* Update Trilium to 0.50.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.50.3)
* add allow robots.txt tag for /share
* fix inclusion of share.js in builds, fixes #2738
* invalidate flat text cache after login to protected session, fixes #2712
* logging improvements
* fix uploading binary data through ETAPI, closes #2667
* raise line-height in share CSS to 1.5, fixes #2671
* fixed blurred selection background color, closes #2652
* make it clear that Dockerfile needs to be run through a script
* fix importing unknown image types such as .DWG, * closes #2628
* fix param validation

[1.5.14]
* Update Trilium to 0.51.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.51.2)
* addTextToEditor appends text to the end instead of the beginning
* fix "isActive()" detection to work well with splits, #2806
* fix doubling of icon tooltips, closes #2811
* allow searching within mermaid diagrams, closes #2821
* allow combining tokens in text and title/attributes, fixes #2820
* improve hiding of edit button #2787
* fix missing closing div tag in word count demo widget, closes #2829
* make sure shaca is loaded before any request

[1.6.0]
* Update Trilium to 0.52.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.52.2)
* new excalidraw backed "canvas note", contributed by @thfrei
* new Find in text widget, based on @antoniotejada's work
* add option to disable auto-download of images for offline storage, #2859
* disable COEP, fixes #2858
* moved protected session expiration scheduling #2855
* added #titleTemplate, closes #2852
* new ~ operator in search for regex
* allow operators =, = and = on note content

[1.6.1]
* Update Trilium to 0.52.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.52.3)
* (Bug report) Restoring note revision throws an exception #2915
* Added the USER_UID & USER_GID env variables #2917

[1.6.2]
* Update Trilium to 0.52.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.52.4)
* This release fixes a security issue

[1.7.0]
* Update Trilium to 0.53.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.53.2)
* added TOC sidebar for text notes (based on @antoniotejada's work)
* Select template when creating page #2813
* Bulk-adding attributes #2812
* New (experimental) note type opening a webview with an external website #2515
* Select Tree Items in Between (Normally Shift) #2647
* fix node size in a note map when note has hidden image notes #2965
* use fenced code block style for markdown export #2963

[1.8.0]
* Update Trilium to 0.54.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.54.2)
* optional basic auth for shared notes, closes #2781
* add highlighting to search results, closes #2977
* allow per workspace calendars, fixes #2959
* added mermaid export SVG button
* export note ZIPs via ETAPI, #3012
* Add config setting to disable update check #3000
* added an option to define a "min TOC headings", #2985
* add #toc label to control Table of Contents visibility per note, #2985
* upgrade excalidraw to 0.12 #2994
* useMaxWidth for mermaid pie widget, fixes #2984

[1.8.1]
* Update Trilium to 0.54.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.54.3)
fixes overwriting attributes after clicking on a relation in the attribute editor, closes #3090
* defensive programming #3089
* trilium safe mode now disables GPU usage
* fix creating virtual "none" note in becca, #3066
* update demo document, closes #3061
* fix falsy check in setting custom widget positions, closes #3060
* fix "show recent note" button, closes #3051
* add manifest.webmanifest into desktop make pwa install on ipad #3048

[1.8.2]
* Update Trilium to 0.55.1
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.55.1)
* Fix max content width not working, fixes #3056
* Fix UTF-8 file name encoding for uploads, fixes #3013
* Fix relation map crash on missing inverse pair, fixes #3055
* Add shareDescription
* Filter excludeFromNoteMap from backlinks
* Add HTTP compression
* Rate limit the /auth/login route of ETAPI
* focus autocomplete on new tab even if there are workspaces, fixes #3083
* remove all alert() usages, fixes #3086
* fix share.js in case there's no menu
* limit max imported file size to 250 MiB, #3108
* add support for the persistent #sortDirection and #sortFoldersFirst (one time UI action exists), closes #3046
* small fixes to tray and closing windows
* focus existing window on port conflict, closes #3036
* remove port scanning for an available port #3036
* added Montserrat-Regular.ttf to fix boldness on mac, closes #3094
* separated editable code buttons into separate widget which also fixes scrolling issue
* added "scrolling-container" class, fixes #3147
* fix erasing notes - becca should be reloaded afterwards, closes #3146
* add file properties widget in mobile layout
* let import continue when malformed URLs are encountered
* bring back the possibility to close the floating buttons again, closes #3116
* ETAPI spec updated
* add #workspaceTemplate which works as workspace-scoped template, closes #3137
* make context sub-menu scrollable, fix #3136
* allow deleting notes from note actions button, closes #3131
* zoom buttons in main menu, closes #2894
* drag & drop from tree will insert links to notes, closes #227
* API log widget

[1.9.0]
* Update Trilium to 0.56.1
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.56.1)
* codemirror updated to 5.65.9
* ckeditor updated to 35.2.1
* update pwa manifest support deploy in sub folder #3199
* fix of Edit button not refreshing correctly upon note type/mime change
* fix toc with > 10 items on the same level, closes #3182
* support basic auth in ETAPI
* improved error handling of wrong port configuration, #3177
* added "#color" label support for notes

[1.9.1]
* Update Trilium to 0.56.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.56.2)
* upgrade mermaid to 9.1.7
* fix deleting note from task manager, closes #3239
* close hanging autocomplete on dialog close, fixes #3241
* allow specifying rootNote for date API functions, #3237
* fix clicking internal link in the ckeditor toolbar, closes #3236

[1.9.2]
* Update Trilium to 0.57.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.57.2)
* enabled advanced list features, fixes #3308
* use CMD/meta key for opening link in a new tab on mac, #3303
* added a context menu for image to copy, #1954
* Remove "Content of this note cannot be displayed" in the book format, fixes #3301
* note about using password to login to web, #3297
* add keyboard action to force creating note revisions, #2147
* note hoisting should be done on "hovered", not active note, closes #2124
* Added option to toggle line wrapping for Code notes
* link map supports search notes
* fix updating note detail after change from another client, closes #3292
* better titles in delete dialog, #3290
* filter edited notes by hoisted note/workspace
* add possibility to define a share index, closes #3265
* fix paste after, closes #3276
* fix refocusing find widget, closes #3252
* use "asset path" in URLs to avoid caching issues after upgrades
* refactored/restructured Options dialog
* Rate limit some more ETAPI auth routes; loginRateLimiter now doesn't count successful auth to ETAPI routes by @DynamoFox
* added option to disable tray, closes #2612
* add "erase all deleted notes now" also to recent changes
* add TRILIUM_NO_UPLOAD_LIMIT to disable the upload limit, #3164
* avoid errors on dead backlinks, #3289
* fix backlink generation for drag & dropped links, fixes #3314
* fix cursor jumping problem when having same note open in two tabs, closes #3365
* each stripped tag will be replace by a space, #3355
* Add Ctrl+PgUp/Dn for tab switching, pointer to Electron docs

[1.9.3]
* Update Trilium to 0.57.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.57.3)
* fixed webmanifest and robots.txt

[1.9.4]
* Update Trilium to 0.57.4
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.57.4)
* Fix missing boxicons icons; Add missing boxicons LICENSE file from up stream by @DynamoFox
* attempt to fix the ws crash, #3374
* dump-db should not contain node_modules in release artifacts
* fix internal link creation, closes #3376

[1.9.5]
* Update Trilium to 0.57.5
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.57.5)
* Fix missing share.js in public/app-dist folder; Delete unused public/app folder (Docker build) #3410 @DynamoFox

[1.10.0]
* Update Trilium to 0.58.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.58.4)
* Options and Backend log are not dialogs anymore, they are converted to "content widgets"
* fix reopening the very last tab, closes #3397
* mobile frontend now contains the launchbar as well and has access to more functionality (e.g. Edit button in read-only notes)
* deajan/fix-powershell-exec-policy #3406
* label update should trigger parent resort, fixes #3366
* upgrades - ckeditor 35.4.0, mermaid 9.3.0, excalidraw 0.13.0
* added watchdog for CKEditor to recover from crashes, fixes #3227

[1.10.1]
* Update Trilium to 0.58.5
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.58.5)
* fix duplicate unique key in migration, #3526
* put firing of the runOnNoteContentChange on entity level instead of service level, #3436
* fix null content when protecting notes, closes #3523
* add runOnNoteContentChange into the autocomplete
* restore all "named" notes quickly after their deletion, #3517

[1.10.2]
* Update Trilium to 0.58.6
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.58.6)
* fix history navigation buttons in server, #3527
* fix note revision for images
* fix loading custom themes in Options, closes #3528

[1.10.3]
* Update Trilium to 0.58.7
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.58.7)
* fix migration for DBs which did not have `_hidden` tree created, closes #3536

[1.10.4]
* Update Trilium to 0.58.8
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.58.8)
* fix hidden subtree appearing after collapse, #2922
* fix image display with slash in title, closes #3591
* fix consistency check to re-parent note from search parent to root
* recover notes into root only if no other valid branch exists, #3590
* Fix broken reference consistency check by @eliandoran
* fix filing entity changes for deleted notes

[1.11.0]
* Update Trilium to 0.59.1
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.59.1)
* (refactoring) Rename entities #3476
* keep note ordering in export #2346
* added "inherit" relation, #3493
* `_hidden` note does not inherit attributes from root, fixes #3537
* add a button to temporarily hide TOC, closes #3555
* reimplement docker healtcheck into node.js to take into account configuration, #3582
* note source now opens in a new tab
* fix autocomplete showing hidden paths, closes #3654
* fix display of note revision content, #3637
* submenu choice of template should override child: settings, fixes #3628
* Fix middle vertical align on table cells #3650 by @eliandoran

[1.11.1]
* Update Trilium to 0.59.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.59.2)
* fix import of ZIP with images with no meta file, closes #3649
* Allow arbitrary labels to be added via web clipper
* #run should respect inheritance, fixes #3669
* content null check in full text search, #3672
* use more precise detection of a mobile browser, fixes #3680
* support listening on unix sockets
* added some extra allowed URI schemes, fixes #3692
* `docker_healthcheck.js` should not create log files, #3677
* Docker non-root healthcheck #3685 by @holo
* Avoid EditableCode inheriting mode from previous notes. @PJB3005
* fix default keyboard shortcuts for mac forward, back, closes #3708

[1.11.2]
* Update Trilium to 0.59.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.59.3)
* fix hangup on highlighting empty tokens, closes #3772
* fix escaping in sql console results
* promoted and inherited attributes should be shown grouped based on the owning note, #3761
* add #newNotesOnTop, closes #3734
* add a check for the hidden note existence, #3728
* fix sanitization of autocomplete against XSS
* fix cache invalidation upon note title change
* fix dead references in `consistency_checks.js`

[1.11.3]
* Update Trilium to 0.59.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.59.4)
* fix displaying error message in mermaid, closes #3841
* download offline images from libreoffice, fixes #3842
* fix duplicating subtree with internal links, closes #3813
* don't update attribute detail while composing CJK characters, fixes #3812
* fix click events propagating from context menu being closed, fixes #3805
* promotes attributes tab should be always visible when available, fixes #3806
* capitalizing ribbon widget names #3806
* awaiting on triggered events/commands in the frontend API, fixes #3799

[1.12.0]
* Update Trilium to 0.60.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.60.3)
* consistent tooltip arrow style, #3948
* selected text in HTML view is searched immediately in find box, #3947
* smooth scrolling for TOC, #3944
* Linux and Windows allow you to choose which app to open the note with ("Open note custom"), #3940
* improved Cyrillic font support, #3933
* move "tree actions" to the right, #3928
* improved include note display, #3920
* more obvious active ribbon item
* ETAPI fixes and improvements, #3908 #3909
* remove HTML tags added by trilium during ZIP import, #3897
* added ability to override default search engine, #3901
* ensure note return branch #3893, thanks to @mirrorcult
* improved performance of note map #3870
* change in regex of resolving custom paths, #3866
* dev env changes #3871 #3827 #3848 #3847 #3846 #3839 #3838 #3833 #3837
* lint changes #3818
* fix click events propagating from a context menu being closed, fixes #3805
* fix resizing of note map #3808
* added #sortLocale, #3695
* don't trigger menu items on right click, #3764
* persistent caching #3814
* sort icons by how much they are used, #3784
* don't trigger menu items on right click, #3764
* change dropdown border to light border
* SVG icons #3773
* note book card margin #3756
* add #newNotesOnTop, closes #3734
* use the same variable as Firefox for scrollbar thumb #3152
* fix search in "view source", closes #3929
* fix cursor position when Jumping from note to included note
* fix parsing the authentication header with password containing a colon, closes #3916
* don't allow setting image quality to empty value, #3894
* fix hamburger icon in canvas, #3780
* wrong password login screen should return 401 so that it counts to the rate limiter, fixes #3867
* collapse command will collapse the whole tree, including the current active note path, fixes #3664
* Merge pull request #3959 from soulsands/fix-search
* images in note list preview should be shrinked to fit into view, fixes #3965 
* #3974 from soulsands/fix-edited-note fix getEditedNotesOnDate
* right click on an external link should not open the link, fixes #3971 
* keep this window on top #3963 by @SiriusXT
* display icon for non-supported note types in content renderer
* fixed infinite recursion with search notes
* if a note context has sub contexts, then it has to be saved even if empty, fixes #3985
* invalidate attribute cache on branch create/update, fixes #3994
* use note size format also in file properties
* Merge pull request #3984 from dymani/move-pane Add buttons to reorder split panes by @dymani
* don't allow patching relation's value in ETAPI #3998
* Fix shortcuts not resetting to default #4004 by @dymani
* fix showing deleted notes in the recent changes dialog, closes #4013
* ETAPI ZIP import
* fix race condition between script execution and saving, closes #4028
* compatibility with online excalidraw tool - JSON can be imported into excalidraw web tool
* sql console outputs results of CTEs, fixes #2800
* allow creating backups via ETAPI, #4014

[1.12.1]
* Update Trilium to 0.60.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.60.4)
* fix keyboard navigation in the note tree, fixes #4036 
* Paste Into Note Keyboard Shortcut No longer Works #4039 (same bug as above) 
* fix notePosition assignment for new children of root

[1.13.0]
* Update base image to 4.2.0

[1.14.0]
* Update Trilium to 0.61.11
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.61.11)

[1.14.1]
* Update Trilium to 0.61.12
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.61.12)

[1.14.2]
* Update Trilium to 0.61.13
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.61.13)

[1.14.3]
* Update Trilium to 0.61.14
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.61.14)
* fix loading reference link titles in read-only text notes, closes #4404 
* fix content renderer of protected canvas/mermaid notes, closes #4414 
* fix assigning extension to attachment export filenames, closes #4415 
* revert general HTML support 

[1.14.4]
* Update Trilium to 0.61.15
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.61.15)
* fix creating tree cycle by moving clone into another clone, closes #4442
* fix incorrect syntax/typo, closes #4443
* fix edit history crossing note activation in canvas, #4437
* fix buggy http proxy initialization, closes #4453

[1.15.0]
* Update Trilium to 0.62.2
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.62.2)
* scroll beyond last line in text/code editor, fixes #2728
* TOC and Highlights widgets have a button for quick access to options
* Added runOnBranchChange event #4108
* improved PWA manifest #4371, thanks to @rauenzi
* Added more backend API methods to access options, attachments, revisions
* reduce indent of TOC when larger headings are not being used, fixes #4363
* note list should show the prefix, #4362
* added runAsyncOnBackendWithManualTransactionHandling API method, runOnBackend checks that a sync method has been passed.
* add promoted attributes widget to the mobile version, closes #4314

[1.15.1]
* Update Trilium to 0.62.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.62.3)
* make sure the promoted attributes don't take the whole screen in mobile, #4468
* fix printing math, closes #4470
* remove title to fix closing highlights list, fixes #4471
* fix thumbnails with chinese titles, closes #4478
* normalize strings before calculation hashes, #4435
* dragging notes from note tree will automatically insert them as images where appropriate (image, canvas, mermaid)
* when canvas and mermaid are inserted using "include note", we insert them as images

[1.15.2]
* Update Trilium to 0.62.4
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.62.4)
* fix "long sync" when erasing revisions

[1.15.3]
* Update Trilium to 0.62.5
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.62.5)
* unescape HTML before downloading images, #4566
* correctly save attachment URL, #4566
* fix auto-download of images, closes #4566
* document attachment ETAPI APIs in OpenAPI spec, fixes #4559
* fix loading katex in share #4558
* convert absolute image attachment URLs to relative without domain, fixes #4509

[1.15.4]
* Update Trilium to 0.62.6
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.62.6)
* optimize migration process #4535
* fix rendering image title in share renderer, closes #4578
* fix URL unescaping in improper place, #4566
* support SVG image upload, fixes #4573
* remove conflicting shortcut, fixes #4570

[1.16.0]
* Update Trilium to 0.63.3
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.63.3)
* add API method to erase a revision #4662
* fix highlighting search results with regexp characters in fulltext string, closes #4665
* Improved scriptes bin/copy-trilium.sh, bin/build-linux-x64.sh #4671
* fix searching fulltext with tags, closes #4661
* #4635 from WantToLearnJapanese/WantToLearnJapanese-patch-unhoist-bookmark
* fix JSDoc, closes #4633
* Add the -o option to the groupmod command used in start-docker.sh #4632
* PWA manifest fixes for extra auth, fixes #4611
* activate parent note when deleting a note, #4600
* added keyboard shortcut for toggling the right pane, closes #4552
* fix pin button shadow, closes #4595
* CKEditor 41.0.0
* synx fixes

[1.16.1]
* Update Trilium to 0.63.5
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.63.5)
* fix rendering of math on non-root paths, closes #4713
* allow cancelling mention UI with escape, fixes #4692, #4684
* fix copy link in browser, closes #4682
* allow converting file attachments to file notes
* fix: invisible unchecked to-do items, by @st3iny

[1.16.2]
* Update Trilium to 0.63.6
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.63.6)

[1.17.0]
* Update Trilium to 0.63.7
* [Full changelog](https://github.com/zadam/trilium/releases/tag/v0.63.7)
* backported Excalidraw upgrade to 0.17.3
* Update base image to 5.0.0

