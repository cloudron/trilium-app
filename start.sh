#!/bin/bash

set -eu

chown -R cloudron:cloudron /app/data

export TRILIUM_DATA_DIR=/app/data
export TRILIUM_PORT=8080

exec /usr/local/bin/gosu cloudron:cloudron /app/code/trilium.sh

